//
//  Github.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 16/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import Foundation
import Moya

enum Github {
  case topSwift(page:Int)
  case pullRequest(author:String, repository:String, page:Int)
}

extension Github: TargetType {
  /// The target's base `URL`.
  var baseURL: URL { return URL(string: "https://api.github.com")! }
  
  /// The path to be appended to `baseURL` to form the full `URL`.
  var path: String {
    switch self {
    case .topSwift:
      return "/search/repositories"
    case .pullRequest(author: let author, repository: let repo, page: _):
      return "/repos/\(author)/\(repo)/pulls"
    }
  }
  
  /// The HTTP method used in the request.
  var method: Moya.Method {
    return .get
  }
  
  /// The parameters to be incoded in the request.
  var parameters: [String: Any]? {
    switch self {
    case .topSwift(page: let page):
      return [
        "q":"language:Swift",
        "sort":"stars",
        "page":page
      ]
    case .pullRequest(author: _, repository: _, page: let page):
      return [
        "page":page
      ]
    }
  }
  
  /// The method used for parameter encoding.
  var parameterEncoding: ParameterEncoding {
    return URLEncoding.default
  }
  
  /// Provides stub data for use in testing.
  var sampleData: Data {
    return "{\"id\":1}".data(using: .utf8)!
  }
  
  /// The type of HTTP task to be performed.
  var task: Task { return .request }
  
  /// Whether or not to perform Alamofire validation. Defaults to `false`.
  var validate: Bool {
    return false
  }
}
