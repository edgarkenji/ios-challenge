//
//  LoadingViewProtocol.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum LoadingState:Int {
  case loading, loaded, error, empty
}

protocol LoadingViewProtocol {
  var state:Variable<LoadingState> { get set }
  
  var disposeBag:DisposeBag { get }
  
  func setupStateHandling()
  
  func setState(_ state:LoadingState)
  func updateUI(state:LoadingState)  
}

extension LoadingViewProtocol {
  func setupStateHandling() {
    state.asObservable()
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { state in
        self.updateUI(state:state)
      })
      .addDisposableTo(disposeBag)
  }
  
  func setState(_ state:LoadingState) {
    self.state.value = state
  }
}
