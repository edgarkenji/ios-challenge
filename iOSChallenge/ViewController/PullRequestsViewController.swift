//
//  PullRequestsViewController.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import SafariServices

class PullRequestsViewController: BaseViewController {

  var providedListViewModel:ProvidedListViewModel<GithubPullRequest, Github> = ProvidedListViewModel()
  
  let provider:RxMoyaProvider<Github> = RxMoyaProvider<Github>(
//    plugins: [NetworkLoggerPlugin()]
  )
  
  var repository:GithubRepository?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    
    self.tableView.estimatedRowHeight = 104
    
    self.navigationItem.title = repository?.name
    
    providedListViewModel.list.asObservable()
      .observeOn(MainScheduler.instance)
      .bindTo(tableView.rx.items(cellIdentifier: "PullRequestCell", cellType: GithubPullRequestTableViewCell.self)) { [unowned self] index, model, cell in
        self.configure(cell: cell, model: model)
      }
      .addDisposableTo(disposeBag)
    
    tableView.rx.modelSelected(GithubPullRequest.self)
      .subscribe(onNext: { [unowned self] (pullRequest) in
        self.openPullRequest(pullRequest: pullRequest)
      })
    .addDisposableTo(disposeBag)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  internal func configure(cell: GithubPullRequestTableViewCell, model:GithubPullRequest) {
    cell.viewModel = GithubPullRequestViewModel(model: model)
  }
  
  override func requestPage(page: Int) {
    guard let author = repository?.owner.username, let repoName = repository?.name else {
      return
    }
    providedListViewModel.request(with: .pullRequest(author: author, repository: repoName, page: page))
      .do(onCompleted: { _ in
        self.refreshControl.endRefreshing()
      })
      .subscribe(onNext: { [unowned self] (list) in
        self.handleRequestResult(isResultEmpty: list.isEmpty, isModelEmpty: self.providedListViewModel.list.value.isEmpty)
      }, onError: { (error) in
        self.handleRequestError(error)
      })
      .addDisposableTo(disposeBag)
  }
  
  private func openPullRequest(pullRequest:GithubPullRequest) {
    guard let url = URL(string: pullRequest.url) else { return }
    let vc = SFSafariViewController(url: url)
    if let color = navigationController?.navigationBar.barTintColor {
      if #available(iOS 10.0, *) {
        vc.preferredBarTintColor = color
      } else {
        // Fallback on earlier versions
        vc.view.tintColor = color
      }
    }
    present(vc, animated: true, completion: nil)
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
