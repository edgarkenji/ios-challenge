//
//  RepositoriesViewController.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 16/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import Moya
import Unbox

class RepositoriesViewController: BaseViewController {
  
  var providedListViewModel:ProvidedListViewModel<GithubRepository, Github> = ProvidedListViewModel()
  
  let provider:RxMoyaProvider<Github> = RxMoyaProvider<Github>(
//    plugins: [NetworkLoggerPlugin()]
  )
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.tableView.estimatedRowHeight = 104

    providedListViewModel.list.asObservable()
      .observeOn(MainScheduler.instance)
      .bindTo(tableView.rx.items(cellIdentifier: "RepositoryCell", cellType: GithubRepositoryTableViewCell.self)) { [unowned self] index, model, cell in
        self.configure(cell: cell, model: model)
      }
      .addDisposableTo(disposeBag)
    
    tableView.rx.modelSelected(GithubRepository.self)
      .asObservable()
      .subscribe(onNext: { [unowned self] (repo) in
        self.performSegue(withIdentifier: AppSegue.showPullRequest.rawValue, sender: repo)
      })
      .addDisposableTo(disposeBag)
   
    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

  }
  
  internal func configure(cell:GithubRepositoryTableViewCell, model:GithubRepository) {
    cell.viewModel = GithubRepositoryViewModel(model: model)
  }
  
  override internal func requestPage(page:Int) {
    providedListViewModel.request(with: .topSwift(page: page))
      .do(onNext: { _ in
        self.refreshControl.endRefreshing()
      }, onError: { _ in
        self.refreshControl.endRefreshing()
      })
      .subscribe(onNext: { [unowned self] (list) in
        self.handleRequestResult(isResultEmpty: list.isEmpty, isModelEmpty: self.providedListViewModel.list.value.isEmpty)
      }, onError: { [unowned self] (error) in
        self.handleRequestError(error)
      })
      .addDisposableTo(disposeBag)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let identifier = segue.identifier else {
      return
    }
    switch identifier {
    case AppSegue.showPullRequest.rawValue:
      guard let model = sender as? GithubRepository else { return }
      if let vc = segue.destination as? PullRequestsViewController {
        vc.repository = model
      }
    default:
      break
    }
  }
}
