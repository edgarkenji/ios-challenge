//
//  BaseViewController.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import UIKit
import RxSwift

class BaseViewController: UIViewController {

  @IBOutlet var tableView:UITableView!
  @IBOutlet weak var loadingView: LoadingView!
  
  var scrollingIndex:Int = 1
  var shouldInfiniteScroll = true
  var isInfiniteScrolling = false
  
  let refreshControl = UIRefreshControl()
  
  let disposeBag = DisposeBag()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.tableView.dataSource = nil
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.tableView.delegate = self

    refreshControl.addTarget(self, action: #selector(performRefresh), for: .valueChanged)
    if #available(iOS 10.0, *) {
      self.tableView.refreshControl = refreshControl
    } else {
      // Fallback on earlier versions
      self.tableView.addSubview(refreshControl)
    }
    
    self.loadingView.reloadButton.rx.tap
      .asDriver()
      .drive(onNext: { [unowned self] _ in
        self.requestInitial()
      })
      .addDisposableTo(disposeBag)
    
    self.requestInitial()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
  private func requestInitial() {
    shouldInfiniteScroll = true
    loadingView.state.value = .loading
    requestPage(page: 1)

  }

  internal func requestPage(page:Int) {
  }
  
  internal func performRefresh() {
    requestPage(page: 1)
  }
  
  internal func handleRequestResult(isResultEmpty:Bool, isModelEmpty:Bool) {
    
    if !isModelEmpty {
      self.loadingView.state.value = .loaded
      if isResultEmpty {
        self.shouldInfiniteScroll = false
      }
    } else {
      self.shouldInfiniteScroll = false
      self.loadingView.state.value = .empty
    }
    self.isInfiniteScrolling = false
  }
  
  internal func handleRequestError(_ error:Error) {
    self.shouldInfiniteScroll = false
    self.isInfiniteScrolling = false
    self.loadingView.messages[.error] = error.localizedDescription
    self.loadingView.state.value = .error
  }
  
}

extension BaseViewController: UITableViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let offsetY = scrollView.contentOffset.y
    let height = scrollView.contentSize.height
    if shouldInfiniteScroll && offsetY > height - scrollView.frame.size.height && !isInfiniteScrolling {
      isInfiniteScrolling = true
      scrollingIndex += 1
      requestPage(page: scrollingIndex)
    }
  }
}

