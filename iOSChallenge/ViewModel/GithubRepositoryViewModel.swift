//
//  GithubRepositoryViewModel.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 16/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import Foundation

class GithubRepositoryViewModel {
  let model:GithubRepository
  
  init(model:GithubRepository) {
    self.model = model
  }
  
  var repoName:String {
    return model.name
  }
  
  var repoDescription:String {
    return model.repoDescription
  }
  
  var stars:String {
    return "\(model.stars)"
  }
  
  var forks:String {
    return "\(model.forks)"
  }
  
  var owner:String {
    return model.owner.username
  }
  
  var photoURL:URL? {
    return URL(string: model.owner.avatarUrl)
  }
}
