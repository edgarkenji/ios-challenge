//
//  ProvidedListViewModel.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import Foundation
import RxSwift
import Unbox
import Moya

class ProvidedListViewModel<Model, ProviderType> where Model:Unboxable, ProviderType:TargetType {
  var list:Variable<[Model]> = Variable([])
  let provider = RxMoyaProvider<ProviderType>(plugins: [NetworkLoggerPlugin()])

  func request(with providerType:ProviderType) -> Observable<[Model]> {
    return provider.request(providerType)
      .filterSuccessfulStatusCodes()
      .asObservable()
      .mapJSON()
      .map({ json -> [Model] in
        if let dicts = json as? [UnboxableDictionary] {
          let list:[Model] = try unbox(dictionaries:dicts)
          return list
        } else if let dict = json as? UnboxableDictionary {
          let list:[Model] = try unbox(dictionary: dict, atKeyPath: "items")
          return list
        } else {
          return []
        }
      })
      .do(onNext: { (list) in
        self.list.value.append(contentsOf: list)
      }, onError: { error in
        NSLog("error \(error)")
      })
  }
}
