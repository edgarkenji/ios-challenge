//
//  GithubPullRequestViewModel.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import Foundation

class GithubPullRequestViewModel {
  let model:GithubPullRequest
  
  init(model:GithubPullRequest) {
    self.model = model
  }
  
  var title:String {
    return model.title
  }
  
  var body:String {
    return model.body
  }
  
  var user:String {
    return model.user.username
  }
  
  var photoURL:URL? {
    return URL(string: model.user.avatarUrl)
  }
  
  var date:String {
    return DateUtils.readableFormatter.string(from: model.date)
  }
}
