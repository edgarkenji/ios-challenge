//
//  GithubPullRequestView.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import UIKit
import NibDesignable
import RxSwift

class GithubPullRequestView: NibDesignable {

  @IBOutlet weak var titleLabel: UILabel!
  
  @IBOutlet weak var bodyLabel: UILabel!
  
  @IBOutlet weak var userProfileView: UserProfileHorizontalView!
  @IBOutlet weak var dateLabel: UILabel!
  
  var viewModel:GithubPullRequestViewModel? = nil {
    didSet {
      if let viewModel = viewModel {
        self.configure(viewModel: viewModel)
      }
    }
  }
  
  var disposeBag = DisposeBag()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  func configure(viewModel:GithubPullRequestViewModel) {
    titleLabel.text = viewModel.title
    bodyLabel.text = viewModel.body
    userProfileView.nameLabel.text = viewModel.user
    dateLabel.text = viewModel.date
  }

}
