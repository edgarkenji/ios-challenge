//
//  GithubPullRequestTableViewCell.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import UIKit

class GithubPullRequestTableViewCell: UITableViewCell {
  @IBOutlet weak var view: GithubPullRequestView!
  
  var viewModel:GithubPullRequestViewModel? {
    didSet {
      if let viewModel = viewModel {
        view.viewModel = viewModel
        view.userProfileView.imageView.kf.setImage(with: viewModel.photoURL)
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    viewModel = nil
    view.viewModel = nil
    view.userProfileView.imageView.kf.cancelDownloadTask()
  }
}
