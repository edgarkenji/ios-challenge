//
//  GithubRepositoryTableViewCell.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 16/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import UIKit
import Kingfisher

class GithubRepositoryTableViewCell: UITableViewCell {
  
  @IBOutlet weak var view: GithubRepositoryView!
  
  var viewModel:GithubRepositoryViewModel? {
    didSet {
      if let viewModel = viewModel {
        view.viewModel = viewModel
        view.userView.imageView.kf.setImage(with: viewModel.photoURL)
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    viewModel = nil
    view.viewModel = nil
    view.userView.imageView.kf.cancelDownloadTask()
  }
}
