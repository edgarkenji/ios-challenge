//
//  GithubRepositoryTableViewCell.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 16/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import UIKit
import NibDesignable
import RxSwift

@IBDesignable
class GithubRepositoryView: NibDesignable {
  
  @IBOutlet weak var repoLabel: UILabel!
  @IBOutlet weak var repoDescriptionLabel: UILabel!
  @IBOutlet weak var repoStatsView: RepoStatsView!
  @IBOutlet weak var userView: UserProfileView!
  
  var viewModel:GithubRepositoryViewModel? = nil {
    didSet {
      if let viewModel = viewModel {
        self.configure(viewModel: viewModel)
      }
    }
  }
  
  var disposeBag = DisposeBag()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  func configure(viewModel:GithubRepositoryViewModel) {
    repoLabel.text = viewModel.repoName
    repoDescriptionLabel.text = viewModel.repoDescription
    userView.nameLabel.text = viewModel.owner
    
    repoStatsView.forksLabel.text = viewModel.forks
    repoStatsView.starsLabel.text = viewModel.stars
  }
}
