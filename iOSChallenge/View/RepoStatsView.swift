//
//  RepoStatsView.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 16/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import UIKit
import NibDesignable

class RepoStatsView: NibDesignable {
  
  @IBOutlet weak var starsLabel: UILabel!
  @IBOutlet weak var forksLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
