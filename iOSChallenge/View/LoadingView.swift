//
//  LoadingView.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import UIKit
import RxSwift
import NibDesignable

@IBDesignable
public class LoadingView: NibDesignable {
  
  @IBOutlet var messageLabel:UILabel!
  @IBOutlet var loadingView:UIActivityIndicatorView!
  @IBOutlet weak var reloadButton: UIButton!
  
  var disposeBag = DisposeBag()
  var state:Variable<LoadingState> = Variable(.loading)
  var messages:[LoadingState:String] = [:]
  
  override public func awakeFromNib() {
    super.awakeFromNib()
    
    messages = [
      .loading: "Carregando, aguarde...",
      .loaded: "",
      .empty: "Nenhum resultado encontrado",
      .error: "Ocorreu um erro, por favor tente novamente mais tarde"
    ]
    
    setupStateHandling()
  }
  
  public override func willMove(toSuperview newSuperview: UIView?) {
    self.messageLabel.text = messages[.loading]
  }
  
}

extension LoadingView: LoadingViewProtocol {
  func updateUI(state: LoadingState) {
    switch state {
    case .loading:
      self.superview?.bringSubview(toFront: self)
      self.isHidden = false
      self.loadingView.startAnimating()
      self.loadingView.isHidden = false
      self.messageLabel.text = messages[state]
      self.reloadButton.isHidden = true
    case .loaded:
      self.superview?.bringSubview(toFront: self)
      self.loadingView.stopAnimating()
      self.loadingView.isHidden = true
      self.isHidden = true
      self.superview?.sendSubview(toBack: self)
      self.reloadButton.isHidden = true
    case .error:
      self.superview?.bringSubview(toFront: self)
      self.isHidden = false
      self.loadingView.stopAnimating()
      self.loadingView.isHidden = true
      self.messageLabel.text = messages[state]
      self.reloadButton.isHidden = false
    case .empty:
      self.superview?.bringSubview(toFront: self)
      self.isHidden = false
      self.loadingView.stopAnimating()
      self.loadingView.isHidden = true
      self.messageLabel.text = messages[state]
      self.reloadButton.isHidden = false
    }
  }
  
}
