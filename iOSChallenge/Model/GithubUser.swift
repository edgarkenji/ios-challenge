//
//  GithubUser.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import Foundation
import Unbox

struct GithubUser {
  var username:String // "login"
  var avatarUrl:String // "avatar_url"
}

extension GithubUser: Unboxable {
  init(unboxer: Unboxer) throws {
    self.username = try unboxer.unbox(key: "login")
    self.avatarUrl = try unboxer.unbox(key: "avatar_url")
  }
}
