//
//  GithubRepository.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 16/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import Foundation
import Unbox

struct GithubRepository {
  var identifier:Int64
  var name:String
  var repoDescription:String
  var owner:GithubUser
  var stars:Int // "stargazers_count"
  var forks:Int
  
  init(identifier: Int64, name:String, description:String, owner:GithubUser, stars:Int, forks:Int) {
    self.identifier = identifier
    self.name = name
    self.repoDescription = description
    self.owner = owner
    self.stars = stars
    self.forks = forks
  }
}

extension GithubRepository : Unboxable {
  init(unboxer: Unboxer) throws {
    self.identifier = try unboxer.unbox(key: "id")
    self.repoDescription = try unboxer.unbox(key: "description")
    self.name = try unboxer.unbox(key: "name")
    self.owner = try unboxer.unbox(keyPath: "owner")
    self.stars = try unboxer.unbox(key: "stargazers_count")
    self.forks = try unboxer.unbox(key: "forks")
  }
}
