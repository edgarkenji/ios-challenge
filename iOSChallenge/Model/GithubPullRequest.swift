//
//  GithubPullRequest.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import Foundation
import Unbox

struct GithubPullRequest {
  var identifier:Int64
  var url:String
  var user:GithubUser
  var title:String
  var body:String
  var date:Date
  
  init(identifier:Int64, url:String, user:GithubUser, title:String, body:String, date:Date) {
    self.identifier = identifier
    self.url = url
    self.user = user
    self.title = title
    self.body = body
    self.date = date
  }
}

extension GithubPullRequest: Unboxable {
  init(unboxer: Unboxer) throws {
    self.identifier = try unboxer.unbox(key: "id")
    self.url = try unboxer.unbox(key: "html_url")
    self.user = try unboxer.unbox(key: "user")
    self.title = try unboxer.unbox(key: "title")
    self.body = try unboxer.unbox(key: "body")
    self.date = try unboxer.unbox(key: "updated_at", formatter: DateUtils.standardFormatter)
  }
}
