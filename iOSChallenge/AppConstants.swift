//
//  AppConstants.swift
//  iOSChallenge
//
//  Created by Edgar Kenji Yamamoto on 17/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import Foundation

enum AppSegue:String {
  case showPullRequest
}
