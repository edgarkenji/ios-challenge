//
//  iOSChallengeTests.swift
//  iOSChallengeTests
//
//  Created by Edgar Kenji Yamamoto on 16/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import XCTest
import Unbox

@testable import iOSChallenge

class iOSChallengeTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testUnboxSingleRepoMock() {
    guard
      let path = Bundle(for: type(of: self)).path(forResource: "MockSingleRepo", ofType: "json") else {
        XCTFail()
        return
    }
    
    let url = URL(fileURLWithPath: path)
    
    do {
      let data = try Data(contentsOf: url)
      let repo:GithubRepository = try unbox(data: data)
      XCTAssert(repo.identifier == 22458259)
      XCTAssert(repo.name == "Alamofire")
      XCTAssert(repo.owner.username == "Alamofire")
      XCTAssert(repo.repoDescription == "Elegant HTTP Networking in Swift")
      XCTAssert(repo.owner.avatarUrl == "https://avatars0.githubusercontent.com/u/7774181?v=3")
      XCTAssert(repo.stars == 22471)
      XCTAssert(repo.forks == 3886)
    } catch let e {
      NSLog("error \(e)")
      XCTFail(e.localizedDescription)
    }
  }
  
  func testUnboxSinglePullRequestMock() {
    guard
      let path = Bundle(for: type(of: self)).path(forResource: "MockSinglePullRequest", ofType: "json") else {
        XCTFail()
        return
    }
    
    let url = URL(fileURLWithPath: path)
    
    do {
      let data = try Data(contentsOf: url)
      let repo:GithubPullRequest = try unbox(data: data)
      XCTAssert(repo.identifier == 110813172)
      XCTAssert(repo.url == "https://github.com/ReactiveCocoa/ReactiveCocoa/pull/3413")
      XCTAssert(repo.user.username == "Burgestrand")
      XCTAssert(repo.user.avatarUrl == "https://avatars0.githubusercontent.com/u/99166?v=3")
      XCTAssert(repo.title == "Allow specifying KVO options for `NSObject.reactive.values(forKeyPath:)`")
      XCTAssert(repo.body == "Original motivation for this is you don't always want the initial value.\r\n\r\nA real-world example is from dealing with an `AVAsset` in `AVFoundation`. When you read `asset.duration` and the duration is not known yet, then `AVAsset` will busy-load asset metadata to give you a value, which can lock up the main thread. You can avoid this by reading the status of the `duration` key using `AVAsynchronousKeyValueLoading`, and then use KVO to deliver updates if the duration is not yet known.")
      
      let date = Date(year: 2017, month: 3, day: 17, hour: 11, minutes: 28, seconds: 12)
      
      XCTAssert(repo.date == date)
    } catch let e {
      NSLog("error \(e)")
      XCTFail(e.localizedDescription)
    }
  }
  
  func testUnboxRequestRepos() {
    let requestExpectation = expectation(description: "request")
    let providedViewModel = ProvidedListViewModel<GithubRepository, Github>()
    var returnedList:[GithubRepository] = []
    let disposable = providedViewModel.request(with: .topSwift(page: 1))
      .subscribe(onNext: { (list) in
        requestExpectation.fulfill()
        returnedList = list
      })

    waitForExpectations(timeout: 10) { (error) in
      if let error = error {
        XCTFail(error.localizedDescription)
      } else {
        XCTAssert(!returnedList.isEmpty)
      }
      disposable.dispose()
    }
  }
  
  func testUnboxRequestPullRequests() {
    let requestExpectation = expectation(description: "request")
    let providedViewModel = ProvidedListViewModel<GithubPullRequest, Github>()
    var returnedList:[GithubPullRequest] = []
    let disposable = providedViewModel.request(with: .pullRequest(author:"ReactiveCocoa", repository: "ReactiveCocoa", page: 1))
      .subscribe(onNext: { (list) in
        requestExpectation.fulfill()
        returnedList = list
      })
    
    waitForExpectations(timeout: 10) { (error) in
      if let error = error {
        XCTFail(error.localizedDescription)
      } else {
        XCTAssert(!returnedList.isEmpty)
      }
      disposable.dispose()
    }
  }
}
