//
//  iOSChallengeUITests.swift
//  iOSChallengeUITests
//
//  Created by Edgar Kenji Yamamoto on 16/03/17.
//  Copyright © 2017 ikusagami. All rights reserved.
//

import XCTest

class iOSChallengeUITests: XCTestCase {
  
  override func setUp() {
    super.setUp()
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication().launch()
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
  }
  
  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }
  
  func testBasicFlow() {
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    let app = XCUIApplication()
    let label = app.staticTexts["Carregando, aguarde..."]
    let exists = NSPredicate(format: "exists == 0")
    
    _ = expectation(for: exists, evaluatedWith: label, handler: nil)
    waitForExpectations(timeout: 5, handler: { error in
      if let error = error {
        XCTFail(error.localizedDescription)
      } else {
        XCUIApplication().tables.cells.staticTexts.matching(identifier: "Alamofire").element(boundBy: 0).tap()
        self.doTestOnPullRequestScreen()
      }
    })
    
  }
  
  func doTestOnPullRequestScreen() {
    let app = XCUIApplication()
    let label = app.navigationBars["Alamofire"]
    let exists = NSPredicate(format: "exists == 1")
    
    _ = expectation(for: exists, evaluatedWith: label, handler: nil)
    
    waitForExpectations(timeout: 5, handler: { error in
      if let error = error {
        XCTFail(error.localizedDescription)
      } else {
        let table = app.tables.element
        XCTAssertTrue(table.exists)
      }
    })
  }
  
}
